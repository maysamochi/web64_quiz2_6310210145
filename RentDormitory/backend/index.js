const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10
const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')
const connection = mysql.createConnection({
    host:'localhost',
    user:'member_admin',
    password:'member_admin',
    database:'RentDormitorySystem'
})
    connection.connect ()
    const express = require('express')
    const app = express()
    const port = 5000

    /*Middleware for Authenticating User Token */
    function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]

    if(token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) =>{
        if (err) { return res.sendStatus(403)}
        else{
            req.user = user
            next()
        }
    })
}


    app.get ("/list_member", (req, res) => {
        let query = "SELECT * from Member";
        connection.query( query, (err, rows) => {
            if (err) {
                 res.json ({
                              "status" : "400",
                              "message" : "Error querying from running db"
                          })
            }else {
                 res.json(rows)
            }
        })
     })



     app.post("/register_member",  (req, res) => {
        let member_name = req.query.member_name
        let member_surname = req.query.member_surname
        let member_username = req.query.member_username
        let member_password = req.query.member_password
    
        bcrypt.hash(member_password, SALT_ROUNDS, (err, hash) => {
            let query = `INSERT INTO Member 
                    (MemberName, MemberSurname, UserName, PassWord, IsAdmin) 
                    VALUES ('${member_name}','${member_surname}',
                    '${member_username}','${hash}',false)`
            console.log(query)
            connection.query( query, (err, rows) => {
                if (err) {
                    console.log(err)
                    res.json ({
                                "status" : "400",
                                "message" : "Error inserting data into db"
                             })
                }else {
                    res.json({
                        "status" : "200",
                        "message" : "Adding new user succesful"
                    })
                } 
            });
        });
    })


    app.post("/register_room", (req, res) => {
        let member_id = req.query.member_id
        let room_id = req.query.room_id
    
        let query = `INSERT INTO Registration
                    (MemberID,RoomID ) 
                    VALUES (${member_id},
                            ${room_id}
                         )`
        console.log(query)
    
        connection.query( query, (err, rows) => {
            if (err) {
                console.log(err)
                 res.json ({
                              "status" : "400",
                              "message" : "Error inserting data into db"
                          })
            }else {
                 res.json({
                    "status" : "200",
                    "message" : "Registering Room succesful"
                 })
            } 
        });
    });



    app.post("/update_member", authenticateToken, (req, res) =>{

        let member_id = req.query.member_id
        let member_name = req.query.member_name
        let member_surname = req.query.member_surname
    
        let query = `UPDATE Member SET
                        MemberName='${member_name}',
                        MemberSurname='${member_surname}'
                        WHERE MemberID=${member_id} `
        console.log(query)
    
        connection.query( query, (err, rows) => {
            if (err) {
                console.log(err)
                 res.json ({
                              "status" : "400",
                              "message" : "Error updatint record"
                          })
            }else {
                 res.json({
                    "status" : "200",
                    "message" : "Updating user succesful"
                 })
            } 
        });
    })


    app.post("/delete_member", authenticateToken, (req, res) =>{

        let member_id = req.query.member_id
    
        let query = `DELETE FROM Member WHERE MemberID=${member_id}`
        console.log(query)
    
        connection.query( query, (err, rows) => {
            if (err) {
                console.log(err)
                 res.json ({
                              "status" : "400",
                              "message" : "Error deleting record"
                          })
            }else {
                 res.json({
                    "status" : "200",
                    "message" : "deleting user succesful"
                 })
            } 
        });
    })


    app.post("/login", (req, res) => {
        let username = req.query.username
        let userpassword = req.query.password
        let query = `SELECT * FROM Member WHERE Username='${username}'`
        connection.query( query, (err, rows) =>{
            if (err) {
                console.log(err)
                res.json ({
                             "status" : "400",
                             "message" : "Error querying from running db"
                         })
            }else {
                let db_password = rows[0].Password
                bcrypt.compare(userpassword, db_password, (err, result) => {
                    if(result){
                        let payload = {
                            "username" : rows[0].Username,
                            "user_id" : rows[0].RunnerID,
                            "IsAdmin" : rows[0].IsAdmin,
                        }
                        console.log(payload)
                        let token = jwt.sign(payload, TOKEN_SECRET, {expiresIn : '1d'})
                        res.send(token)
                    }else {res.send("Invalid username / password")}
                }) 
           }
        })
    
    }) 


     app.listen(port, () => {
        console.log( `Now starting Running System Backend ${port}`)
     })